from smbus2 import SMBusWrapper

with SMBusWrapper(1) as bus:
    b = bus.read_byte_data(80, 0)
    print(b)
