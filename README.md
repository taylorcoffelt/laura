# Laura - Python Wrapper for Ausus Aura in Linux

A python wrapper for controlling Asus Aura devices on the SMBus(i2c) bus on Linux

---

## Setup

You'll need the following:

`pip install smbus2`

`sudo chmod a+rw /dev/i2c-*`
